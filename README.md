# Still Images Text Detection
### Author: Gustavo Bartholomeu Trad Souza
### Nº USP: 11219216

**Digital Images Processing Course final assignment.** 

This project aims to create a program capable of detecting texts in still images and use some image enhancement techniques to facilitate this detection. The network used to detect the text in the image will be the pre-trained network EAST.

Before being passed to the network to create the text bound boxes, the image will be passed to processes such as sharpening, denoising and debluring with the objective of facilitating the text detection. 


The libraries used int the project are:
* numpy: https://numpy.org/
* OpenCV: https://pypi.org/project/opencv-python/
* Matplotlib: https://matplotlib.org/

* EAST text detector papper: https://arxiv.org/abs/1704.03155

YpuTube Video link: https://youtu.be/IJLgl8IpeQs
