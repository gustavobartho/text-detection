### Final Report

#### Name: Gustavo Bartholomeu Trad Souza
#### Nº USP: 11219216

### **Final proposal**
The final objective of the project is to create bounding boxes arround the text present in still images. Before identifying the text regions, the image is passed to image enhancement functions so that texts in noisy images, that could not be identified before the enhancement, will now be identified. First image enhancement techniques are applyed in the image in order to reduce the noise in the image, the next step is to apply debluring thechniques to improve the image quality and then the image is fed to the network and the text presence information is acquired and used to draw the bounding boxes around the text.

## **Metodology**
The techniques used in the enhancement step are:

 * **Median Filter:** Apply a filter of shape k x k in the image where the resulting pixel is the median of the neighborhood pixels. This filter aims to reduce the noise present in the image by reducing the variance between a pixel and its neighbourhood.
  
 * **Bilateral Filter:** Apply a nonlinear filter to smooth images while preserving edges. This filter aims to reduce the noise and preserve the text edges
 
 * **Unsharp mask using the Laplacian Filter:** The unsharp mask filtering is a sharpening technique that subtracts an unsharp or blurred version of an image from the original image. The objective of this technique is to enhance edges and transitions of intensities.

After the enhancement techniques the image is passed trough a debluring function that aims to improve the image quality and make the text easier to detect.
Next the image is fed to a neural network that outputs as its final layers the information about the presence of text in a given region and the distance of that region to the limits of the bounding box containig it. With this information the bounding boxes are obtained for the regions where text was found.

## **Conclusion**
The objective of the project was reached. After applying the enhancement techniques, the text in noisy images was more easily detected, the notebook in the repository shows the code used and a example with a comparison between an modified image and a unmodifie image.


Youtube Video: https://youtu.be/IJLgl8IpeQs