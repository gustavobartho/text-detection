### Partial Report on the Text Detection Project

#### Name: Gustavo Bartholomeu Trad Souza
#### Nº USP: 11219216 

* **Final proposal:** The final objective of the project will be to create bounding boxes arround the text present in still images. Before identifying the text regions, the image will be passed to image enhancement functions so that texts in noisy images that could not be identified before the enhancement will now be identified. First the image will be passed to noise reduction and debluring functions in order to improve the overall quality of the imge, the next step is to pass the image in sharpening filters so that the borders of the text becomes more distinguishable from its background and then the image will be fed to the network and the text presence information will be acquired and used to draw the bounding boxes around the text.

* The images used in the project will be bad quality images so that the network won't be able to detect the text in them without the image enhancement and restoration procedures. An example of the text detection prcedure in a bad quality image is present in the "Text_Detection.ipynb" Jupyter Notebook. In this example the text could not be detected in the original image, but after it goes through the quality improval processes the text in the image is detected.

* The next step is to create the enhancement and restoration functions so that the text in the images becomes more clear. The "images/text_4.png" is an image that text can't be identified by the network without the proper enhancement and restoration techniques
